/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Datos.BDColaboradores;
import Objetos.ObjColaboradores;
import java.io.File;
import java.util.ArrayList;
import javax.swing.JOptionPane;
/**
 *
 * @author Lukestas
 */
public class Colaboradores {
    BDColaboradores bdcolaboradores= new BDColaboradores();
    
    public ArrayList<ObjColaboradores> leerobjColaboradores(){
        ArrayList<ObjColaboradores> listacolaboradores= bdcolaboradores.LeerObjetoColaboradoreses();
        return listacolaboradores;
    }
    
    
    public void InsertarColaborador(ArrayList<ObjColaboradores> listaColaboradores){
        String datos="";
        for (int i=0;i<=listaColaboradores.size()-1;i++){
            String Nombre= listaColaboradores.get(i).getNombre();
            String Cedula= listaColaboradores.get(i).getCedula();
            String Edad= listaColaboradores.get(i).getEdad();
            String Genero= listaColaboradores.get(i).getGenero();
            String NivelIng= listaColaboradores.get(i).getNivelIng();
            String Departamento= listaColaboradores.get(i).getDepartamento();
            String Puesto= listaColaboradores.get(i).getPuesto();
            String FechaNac= listaColaboradores.get(i).getFechaNac();
            String FechaIng= listaColaboradores.get(i).getFechaIng();
            datos=Cedula+","+Nombre+","+Edad+","+Genero+
                    ","+NivelIng+","+Departamento+","+Puesto+
                    ","+FechaNac+","+FechaIng;
        }
        bdcolaboradores.InsertarColaboradoresArchivo(datos);
    }
}
