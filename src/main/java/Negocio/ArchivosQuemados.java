/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Datos.BDArchivosQuemados;
import java.util.ArrayList;

/**
 *
 * @author Lukestas
 */
public class ArchivosQuemados {
    BDArchivosQuemados bdArchivosQuemados= new BDArchivosQuemados();
    
    public ArrayList<String> LeerArchivoDepartamentos() {
        ArrayList<String> depas = bdArchivosQuemados.LeerDesdeArchivoDepastamentos();
        return depas;
    }
    public ArrayList<String> LeerArchivoPuestos() {
        ArrayList<String> Puestos = bdArchivosQuemados.LeerDesdeArchivoPuestos();
        return Puestos;
    }
}
