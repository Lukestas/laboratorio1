/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Objetos;

import java.util.ArrayList;

/**
 *
 * @author Lukestas
 */
public class ObjColaboradores {

    private String Cedula;
    private String Nombre;
    private String Edad;
    private String Genero;
    private String NivelIng;
    private String Departamento;
    private String Puesto;
    private String FechaNac;
    private String FechaIng;

    public ObjColaboradores(String Cedula, String Nombre, String Edad, String Genero, String NivelIng, String Departamento, String Puesto, String FechaNac, String FechaIng) {
        this.Cedula = Cedula;
        this.Nombre = Nombre;
        this.Edad = Edad;
        this.Genero = Genero;
        this.NivelIng = NivelIng;
        this.Departamento = Departamento;
        this.Puesto = Puesto;
        this.FechaNac = FechaNac;
        this.FechaIng = FechaIng;
    }

    public static ArrayList listaColaboradores= new ArrayList<>();
    
    /**
     * @return the Cedula
     */
    public String getCedula() {
        return Cedula;
    }

    /**
     * @param Cedula the Cedula to set
     */
    public void setCedula(String Cedula) {
        this.Cedula = Cedula;
    }

    /**
     * @return the Nombre
     */
    public String getNombre() {
        return Nombre;
    }

    /**
     * @param Nombre the Nombre to set
     */
    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    /**
     * @return the Edad
     */
    public String getEdad() {
        return Edad;
    }

    /**
     * @param Edad the Edad to set
     */
    public void setEdad(String Edad) {
        this.Edad = Edad;
    }

    /**
     * @return the Genero
     */
    public String getGenero() {
        return Genero;
    }

    /**
     * @param Genero the Genero to set
     */
    public void setGenero(String Genero) {
        this.Genero = Genero;
    }

    /**
     * @return the NivelIng
     */
    public String getNivelIng() {
        return NivelIng;
    }

    /**
     * @param NivelIng the NivelIng to set
     */
    public void setNivelIng(String NivelIng) {
        this.NivelIng = NivelIng;
    }

    /**
     * @return the Departamento
     */
    public String getDepartamento() {
        return Departamento;
    }

    /**
     * @param Departamento the Departamento to set
     */
    public void setDepartamento(String Departamento) {
        this.Departamento = Departamento;
    }

    /**
     * @return the Puesto
     */
    public String getPuesto() {
        return Puesto;
    }

    /**
     * @param Puesto the Puesto to set
     */
    public void setPuesto(String Puesto) {
        this.Puesto = Puesto;
    }

    /**
     * @return the FechaNac
     */
    public String getFechaNac() {
        return FechaNac;
    }

    /**
     * @param FechaNac the FechaNac to set
     */
    public void setFechaNac(String FechaNac) {
        this.FechaNac = FechaNac;
    }

    /**
     * @return the FechaIng
     */
    public String getFechaIng() {
        return FechaIng;
    }

    /**
     * @param FechaIng the FechaIng to set
     */
    public void setFechaIng(String FechaIng) {
        this.FechaIng = FechaIng;
    }

}
