/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presentacion;

import Negocio.*;
import Objetos.*;
import Presentacion.Modificar.modificarinformacion;
import Presentacion.Registros.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Lukestas
 */
public class Programa extends javax.swing.JFrame {

    Colaboradores coop = new Colaboradores();
    ArchivosQuemados Archivos = new ArchivosQuemados();

    public Programa() {
        initComponents();
        ExtraerArchivosDepartamentosYPuestos();
        CargaEnTablaObj();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        grbGenero = new javax.swing.ButtonGroup();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        txtCedula = new javax.swing.JTextField();
        txtNombre = new javax.swing.JTextField();
        rbFemenino = new javax.swing.JRadioButton();
        rbMasculino = new javax.swing.JRadioButton();
        DateFechaNac = new com.toedter.calendar.JDateChooser();
        jLabel7 = new javax.swing.JLabel();
        txtEdad = new javax.swing.JTextField();
        btnInsertar = new javax.swing.JToggleButton();
        DateFechaIng = new com.toedter.calendar.JDateChooser();
        jLabel8 = new javax.swing.JLabel();
        CbDepartamentos = new javax.swing.JComboBox<>();
        jLabel9 = new javax.swing.JLabel();
        CbIngles = new javax.swing.JComboBox<>();
        jLabel10 = new javax.swing.JLabel();
        CBPuesto = new javax.swing.JComboBox<>();
        jScrollPane1 = new javax.swing.JScrollPane();
        TblRegistro = new javax.swing.JTable();
        btnRecarga = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        itemdepas = new javax.swing.JMenuItem();
        itemingles = new javax.swing.JMenuItem();
        itemedad = new javax.swing.JMenuItem();
        itemingreso = new javax.swing.JMenuItem();
        jMenu3 = new javax.swing.JMenu();
        jMenu2 = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setText("Registro de nuevo colaborador");

        jLabel2.setText("Cédula:");

        jLabel3.setText("Nombre:");

        jLabel4.setText("Género:");

        jLabel5.setText("Departamento:");

        jLabel6.setText("Fecha de nacimiento:");

        grbGenero.add(rbFemenino);
        rbFemenino.setText("Femenino");

        grbGenero.add(rbMasculino);
        rbMasculino.setText("Masculino");

        DateFechaNac.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                DateFechaNacPropertyChange(evt);
            }
        });

        jLabel7.setText("Edad:");

        txtEdad.setEditable(false);

        btnInsertar.setText("Insertar");
        btnInsertar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnInsertarActionPerformed(evt);
            }
        });

        jLabel8.setText("Fecha de ingreso:");

        jLabel9.setText("Nivel de ingles:");

        CbIngles.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "A1", "A2", "B1", "B2", "C1" }));

        jLabel10.setText("Puesto:");

        TblRegistro.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "Cedula", "Nombre", "Edad", "Género", "Nivel de Inglés", "Departamento", "Puesto", "Fecha de Nacimiento", "Fecha de Ingreso"
            }
        ));
        jScrollPane1.setViewportView(TblRegistro);

        btnRecarga.setText("Recargar Tabla");
        btnRecarga.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRecargaActionPerformed(evt);
            }
        });

        jMenu1.setText("Registros");

        itemdepas.setText("Departamentos");
        itemdepas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemdepasActionPerformed(evt);
            }
        });
        jMenu1.add(itemdepas);

        itemingles.setText("Ingles");
        itemingles.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                iteminglesActionPerformed(evt);
            }
        });
        jMenu1.add(itemingles);

        itemedad.setText("Edad");
        itemedad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemedadActionPerformed(evt);
            }
        });
        jMenu1.add(itemedad);

        itemingreso.setText("Ingreso");
        itemingreso.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemingresoActionPerformed(evt);
            }
        });
        jMenu1.add(itemingreso);

        jMenuBar1.add(jMenu1);

        jMenu3.setText("Modificar");
        jMenu3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jMenu3MouseClicked(evt);
            }
        });
        jMenuBar1.add(jMenu3);

        jMenu2.setText("Salir");
        jMenu2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jMenu2MouseClicked(evt);
            }
        });
        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(btnRecarga)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel2)
                            .addComponent(jLabel7)
                            .addComponent(jLabel9)
                            .addComponent(btnInsertar))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(txtCedula, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(rbFemenino)
                                .addGap(18, 18, 18)
                                .addComponent(rbMasculino))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txtNombre)
                                    .addComponent(txtEdad)
                                    .addComponent(CbIngles, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel6)
                                    .addComponent(jLabel8)
                                    .addComponent(jLabel5)
                                    .addComponent(jLabel10))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(DateFechaIng, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(DateFechaNac, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(CbDepartamentos, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(CBPuesto, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                .addGap(323, 323, 323))
            .addGroup(layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 994, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtCedula, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4)
                    .addComponent(rbFemenino)
                    .addComponent(rbMasculino))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5)
                    .addComponent(CbDepartamentos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(txtEdad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10)
                    .addComponent(CBPuesto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel9)
                            .addComponent(CbIngles, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnInsertar)
                            .addComponent(btnRecarga)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(DateFechaNac, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel6))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(DateFechaIng, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel8))))
                .addGap(17, 17, 17)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(20, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenu2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenu2MouseClicked
        System.exit(0);
    }//GEN-LAST:event_jMenu2MouseClicked

    private void btnInsertarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnInsertarActionPerformed
        String Cedula = txtCedula.getText();
        String Nombre = txtNombre.getText().toUpperCase();
        String Genero = DeterminarGenero().toUpperCase();
        String Edad = txtEdad.getText();
        String NivelIng = CbIngles.getSelectedItem().toString();
        String Departamento = CbDepartamentos.getSelectedItem().toString();
        String Puesto = CBPuesto.getSelectedItem().toString();
        String FechaNac = fechanacimientoenstring();
        String FechaIng = fechaingresoenstring();
        ObjColaboradores.listaColaboradores.add(new ObjColaboradores(Cedula, Nombre, Edad, Genero, NivelIng, Departamento, Puesto, FechaNac, FechaIng));
        coop.InsertarColaborador(ObjColaboradores.listaColaboradores);
        ObjColaboradores.listaColaboradores = new ArrayList<>();
        CargaEnTablaObj();
    }//GEN-LAST:event_btnInsertarActionPerformed

    private void DateFechaNacPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_DateFechaNacPropertyChange
        try {
            txtEdad.setText(ObtenerEdad());
        } catch (Exception e) {

        }
    }//GEN-LAST:event_DateFechaNacPropertyChange

    private void itemdepasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemdepasActionPerformed
        RegistrosDepartamentos registrodepartamentos = new RegistrosDepartamentos(this, true);
        registrodepartamentos.pack();
        registrodepartamentos.setVisible(true);
    }//GEN-LAST:event_itemdepasActionPerformed

    private void iteminglesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_iteminglesActionPerformed
        RegistrosNivelInglesMujeres registroingles = new RegistrosNivelInglesMujeres(this, true);
        registroingles.pack();
        registroingles.setVisible(true);
    }//GEN-LAST:event_iteminglesActionPerformed

    private void itemingresoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemingresoActionPerformed
        RegistrosIngreso registroingreso = new RegistrosIngreso(this, true);
        registroingreso.pack();
        registroingreso.setVisible(true);
    }//GEN-LAST:event_itemingresoActionPerformed

    private void itemedadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemedadActionPerformed
        RegistrosEdad registroedad = new RegistrosEdad(this, true);
        registroedad.pack();
        registroedad.setVisible(true);
    }//GEN-LAST:event_itemedadActionPerformed

    private void jMenu3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenu3MouseClicked
        modificarinformacion info = new modificarinformacion(this, true);
        info.pack();
        info.setVisible(true);
    }//GEN-LAST:event_jMenu3MouseClicked

    private void btnRecargaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRecargaActionPerformed
        ObjColaboradores.listaColaboradores = new ArrayList<>();
        CargaEnTablaObj();
    }//GEN-LAST:event_btnRecargaActionPerformed

    private void ExtraerArchivosDepartamentosYPuestos() {
        ArrayList<String> Depas = Archivos.LeerArchivoDepartamentos();
        for (String dato : Depas) {
            CbDepartamentos.addItem(dato);
        }
        ArrayList<String> Puestos = Archivos.LeerArchivoPuestos();
        for (String dato : Puestos) {
            CBPuesto.addItem(dato);
        }
    }

    private void CargaEnTablaObj() {
        DefaultTableModel Modelo = (DefaultTableModel) TblRegistro.getModel();
        Modelo.setRowCount(0);
        ArrayList<ObjColaboradores> personas = coop.leerobjColaboradores();
        Object[] arreglo = new Object[9];
        for (int i = 0; i < personas.size(); i++) {
            arreglo[0] = personas.get(i).getCedula();
            arreglo[1] = personas.get(i).getNombre();
            arreglo[2] = personas.get(i).getEdad();
            arreglo[3] = personas.get(i).getGenero();
            arreglo[4] = personas.get(i).getNivelIng();
            arreglo[5] = personas.get(i).getDepartamento();
            arreglo[6] = personas.get(i).getPuesto();
            arreglo[7] = personas.get(i).getFechaNac();
            arreglo[8] = personas.get(i).getFechaIng();
            Modelo.addRow(arreglo);
        }
    }

    private String DeterminarGenero() {
        String dato = "";
        if (rbFemenino.isSelected()) {
            dato = "Femenino";
        } else if (rbMasculino.isSelected()) {
            dato = "Masculino";
        }
        return dato;
    }

    private String ObtenerEdad() {
        DateTimeFormatter formato = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate fechaNacimiento = LocalDate.parse(fechanacimientoenstring(), formato);
        Period edad = Period.between(fechaNacimiento, LocalDate.now());
        String diferencia = edad.getYears() + "";
        return diferencia;
    }

    private String fechanacimientoenstring() {
        Date fecha = DateFechaNac.getDate();
        DateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
        String fechaconvertida = formato.format(fecha);
        return fechaconvertida;
    }

    private String fechaingresoenstring() {
        Date fecha = DateFechaIng.getDate();
        DateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
        String fechaconvertida = formato.format(fecha);
        return fechaconvertida;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Programa.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Programa.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Programa.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Programa.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Programa().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> CBPuesto;
    private javax.swing.JComboBox<String> CbDepartamentos;
    private javax.swing.JComboBox<String> CbIngles;
    private com.toedter.calendar.JDateChooser DateFechaIng;
    private com.toedter.calendar.JDateChooser DateFechaNac;
    private javax.swing.JTable TblRegistro;
    private javax.swing.JToggleButton btnInsertar;
    private javax.swing.JButton btnRecarga;
    private javax.swing.ButtonGroup grbGenero;
    private javax.swing.JMenuItem itemdepas;
    private javax.swing.JMenuItem itemedad;
    private javax.swing.JMenuItem itemingles;
    private javax.swing.JMenuItem itemingreso;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JRadioButton rbFemenino;
    private javax.swing.JRadioButton rbMasculino;
    private javax.swing.JTextField txtCedula;
    private javax.swing.JTextField txtEdad;
    private javax.swing.JTextField txtNombre;
    // End of variables declaration//GEN-END:variables
}
