/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Lukestas
 */
public class BDArchivosQuemados {
    public ArrayList<String> LeerDesdeArchivoDepastamentos() {
        ArrayList<String> lista = new ArrayList<>();
        try {
            File archivo = new File("Departamentos.txt");
            BufferedReader carga = new BufferedReader(new FileReader(archivo));
            while (carga.ready()) {
                lista.add(carga.readLine());
            }
            carga.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al leer del archivo Departamentos",
                    "Mensaje de error", JOptionPane.ERROR_MESSAGE);
        }
        return lista;
    }
    
    public ArrayList<String> LeerDesdeArchivoPuestos() {
        ArrayList<String> lista = new ArrayList<>();
        try {
            File archivo = new File("Puestos.txt");
            BufferedReader carga = new BufferedReader(new FileReader(archivo));
            while (carga.ready()) {
                lista.add(carga.readLine());
            }
            carga.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al leer del archivo Puestos",
                    "Mensaje de error", JOptionPane.ERROR_MESSAGE);
        }
        return lista;
    }
}
