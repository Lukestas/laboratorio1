/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;


import Objetos.ObjColaboradores;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import javax.swing.JOptionPane;
/**
 *
 * @author Lukestas
 */
public class BDColaboradores {
    public void InsertarColaboradoresArchivo(String datosUsuario){
        try{
            File archivo = new File("Colaboradores.txt");
            BufferedWriter carga= new BufferedWriter(new BufferedWriter(new FileWriter(archivo, true)));
            carga.write(datosUsuario+"\r\n");
            carga.close();
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "Error al escribir en el archivo", "Mensaje de error", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public ArrayList<ObjColaboradores> LeerObjetoColaboradoreses(){
        try {
            File archivo = new File("Colaboradores.txt");
            BufferedReader archi = new BufferedReader(new FileReader(archivo));
            while (archi.ready()) {
                String[] separar = new String[3];
                separar = archi.readLine().split(",");
                ObjColaboradores.listaColaboradores.add(new ObjColaboradores(separar[0],separar[1],separar[2],separar[3],separar[4],separar[5],separar[6],separar[7],separar[8]));
            }
            archi.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al leer del archivo",
                    "Mensaje de error", JOptionPane.ERROR_MESSAGE);
        }
        return ObjColaboradores.listaColaboradores;
    }
    
    
    
    public ArrayList<String> LeerColaboradoresArchivo(){
        ArrayList<String> lista = new ArrayList<>();
        try {
            File archivo = new File("Colaboradores.txt");
            BufferedReader carga= new BufferedReader(new FileReader(archivo));
            while (carga.ready()){
                lista.add(carga.readLine());
            }
            carga.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al leer el archivo", "Mensaje de error", JOptionPane.ERROR_MESSAGE);
        }
        return lista;
    }
}
